from connmysql import Database
from documentspdf import DocumentsPDF
from docexcel import DocExcel


class ReslutDB:

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self

    def toresult(self, koneksi, querys1, typenya, dockey, headerpar, header_Row_col, value_Row_col, datefrom, dateto,
                 typesnya, baseurls, pathFile):
        # print(pathFile)
        if typenya == 0:
            with Database(koneksi) as db:
                records = db.query(querys1)
                db.close()
                return records
        elif typenya == '1':
            with Database(koneksi) as db:
                records = db.query(querys1)
                db.close()
                with DocExcel(datefrom, dateto, typesnya) as xls:
                    xls.toExcel(headerpar, dockey, records, header_Row_col, value_Row_col)
                with DocumentsPDF() as pdf:
                    pdf.toPDF(headerpar, dockey, records, datefrom, dateto, typesnya, baseurls, pathFile)
            _namafile = "Report_" + typesnya + "_" + datefrom + "_" + dateto + ".pdf"
            _fName = _namafile.replace(" ", "_").replace(":", "_").replace("-", "_")
            _fullpath = baseurls + pathFile + _fName
            return _fullpath
