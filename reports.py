from flask.json import jsonify
from querydb import QueryDB



class ShowReports:
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self

    def getData(self, params):
        with QueryDB() as qtabs:
            records = qtabs.report(params)
            print(records)
            return jsonify(records)
