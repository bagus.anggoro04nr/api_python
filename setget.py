from datetime import datetime


class SetGet:
    def __init__(self):
        self._datetimefroms = 'null'
        self._datetimes = 'null'
        self._type = 'null'

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self

    def get__datetimes(self):
        return self._datetimes

    def set__datetimes(self, a):
        if a != 'null':
            self._datetimes = a + ' 23:59:59'
        else:
            self._datetimes = datetime.now().strftime('%Y-%m-%d %H:%M:%S')


    def get__datetimefroms(self):
        return self._datetimefroms

    def set__datetimefroms(self, f):
        if f != 'null':
            self._datetimefroms = f + ' 00:00:00'
        else:
            self._datetimefroms = datetime.now().strftime('%Y-%m-%d 00:00:00')


    def get__type(self):
        return self._type

    def set__type(self, t):
        self._type = t


