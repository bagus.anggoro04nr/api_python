import xlsxwriter
from xlsx2html import xlsx2html

class DocExcel:

    def __init__(self, datefrom, dateto, types):
        self._namafile = "Report_" + types + "_" + datefrom + "_" + dateto + ".xlsx"
        self._namafilehtml = "Report_" + types + "_" + datefrom + "_" + dateto + ".html"
        self._pathFile = "C:\\AppServ\\www\\doc_myapp\\"
        self._datefrom = datefrom
        self._dateto = dateto
        self._types = types

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self

    def toExcel(self, paramsheaders, paramskeys, paramsnya, header_Row_col, value_Row_col):
        paramnya = paramskeys.format(":g")
        heads = paramsheaders.format(":g").split(", ")

        keynya = ''
        namaFiles = self._namafile.replace(" ", "_").replace(":", "_").replace("-", "_")
        # print(namaFiles)
        workbook = xlsxwriter.Workbook(self._pathFile + namaFiles)
        worksheet = workbook.add_worksheet()

        header_cell = workbook.add_format()
        header_cell.set_pattern(1)  # This is optional when using a solid fill.
        header_cell.set_bg_color('#006994')
        header_cell.set_bold()
        header_cell.set_align('center')
        # header_cell.set_font_color('red')
        total_cell = workbook.add_format()
        total_cell.set_pattern(1)  # This is optional when using a solid fill.
        total_cell.set_bg_color('pink')
        total_cell.set_bold()

        money_total_cell = workbook.add_format()
        money_total_cell.set_pattern(1)  # This is optional when using a solid fill.
        money_total_cell.set_bg_color('pink')
        money_total_cell.set_bold()
        money_total_cell.set_num_format(3)

        totalall_cell = workbook.add_format()
        totalall_cell.set_pattern(1)  # This is optional when using a solid fill.
        totalall_cell.set_bg_color('lime')
        totalall_cell.set_bold()

        money_totalall_cell = workbook.add_format()
        money_totalall_cell.set_pattern(1)  # This is optional when using a solid fill.
        money_totalall_cell.set_bg_color('lime')
        money_totalall_cell.set_bold()
        money_totalall_cell.set_num_format(3)

        value_cell = workbook.add_format()
        value_cell.set_bg_color('#ff9817')
        value_cell.set_pattern(1)
        value_cell.set_bold()

        money = workbook.add_format()
        money.set_num_format(3)
        money.set_bold()

        header = header_Row_col.split(", ")
        value = value_Row_col.split(", ")
        row = int(header[0])
        col = int(header[1])
        rows = int(value[0])
        cols = int(value[1])
        rows1 = int(value[0])
        rows2 = any
        cols1 = int(value[1])
        count = 0
        countx = 0
        ttlAllQty = 0
        ttlAllHarga = 0
        for header in heads:
            worksheet.write(row, col, header, header_cell)
            col += 1

        for key in range(len(paramsnya)):
            kode_po = paramsnya[key]['kode_po']
            if keynya != kode_po:
                count += 1
                worksheet.write(rows, cols, count, value_cell)
                worksheet.write(rows, cols + 1, paramsnya[key]['kode_po'], value_cell)
                worksheet.write(rows, cols + 2, paramsnya[key]['customer'], value_cell)
                worksheet.write(rows, cols + 3, paramsnya[key]['alamat_cust'], value_cell)
                countx = rows
                if count >= 2:
                    countx = rows + 1
                for key1 in range(len(paramsnya)):
                    kode_ponya = paramsnya[key1][paramnya]
                    if kode_ponya == kode_po:
                        worksheet.write(rows1, cols1 + 4, paramsnya[key1]['nama_barang'])
                        worksheet.write(rows1, cols1 + 5, paramsnya[key1]['jmlh_barang'], money)
                        worksheet.write(rows1, cols1 + 6, paramsnya[key1]['harga_beli'], money)
                        worksheet.write(rows1, cols1 + 7, '=(G' + str(rows1+1) + '*H' + str(rows1+1) + ')', money)
                        ttlAllQty += paramsnya[key1]['jmlh_barang']
                        ttlAllHarga += paramsnya[key1]['harga_beli'] * paramsnya[key1]['jmlh_barang']
                        rows1 += 1
                        rows2 = rows1
                    worksheet.write(rows2 + 1, cols + 4, 'Total Quantity', total_cell)
                    worksheet.write(rows2 + 1, cols + 5, '=SUM(G' + str(countx) + ':G' + str(rows2 + 1) + ')',
                                    money_total_cell)
                    worksheet.write(rows2 + 1, cols + 6, 'Total Harga', total_cell)
                    worksheet.write(rows2 + 1, cols + 7, '=SUM(I' + str(countx) + ':I' + str(rows2 + 1) + ')',
                                    money_total_cell)
            rows1 = rows2 + 1
            rows = rows1
            keynya = paramsnya[key]['kode_po']
        # Write a total using a formula.
        worksheet.write(rows, cols1 + 4, 'All Total Quantity', totalall_cell)
        worksheet.write(rows, cols1 + 5, ttlAllQty, money_totalall_cell)
        worksheet.write(rows, cols1 + 6, 'All Total Harga', totalall_cell)
        worksheet.write(rows, cols1 + 7, ttlAllHarga, money_totalall_cell)

        workbook.close()

        namaFileshtml = self._namafilehtml.replace(" ", "_").replace(":", "_").replace("-", "_")
        self.xls_to_html(self._pathFile + namaFiles, self._pathFile + namaFileshtml)
        # return self._pathFile + self._namafile

    def xls_to_html(self, sourcefile, outputfile):
        xlsx2html(sourcefile, outputfile)


