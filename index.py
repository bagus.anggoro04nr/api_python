from flask import Flask
from flask_restful import Resource, Api, request
from flask_cors import CORS
from upload import Uploads
from login import Authenticate
from menu import menu
from showtable import ShowTables
from reports import ShowReports
from updates import SetData

app = Flask(__name__)
api = Api(app)
CORS(app)

# @app.route("/authenticate", methods=['POST'])


class logins(Resource):
	def post(self):
		request.get_data()
		with Authenticate() as auth:
			return auth.getData(request.data.decode())


class upload1(Resource):
	def post(self):
		request.get_data()
		with Uploads() as upld:
			return upld.getData(request.data.decode())

class menunya(Resource):
	def post(self):
		request.get_data()
		with menu() as mn:
			return mn.getData(request.data.decode())

class showtablenya(Resource):
	def post(self):
		request.get_data()
		with ShowTables() as st:
			data = st.getData(request.data.decode())
			return data

class reportnya(Resource):
	def post(self):
		request.get_data()
		with ShowReports() as rt:
			return rt.getData(request.data.decode())

class updatenya(Resource):
	def post(self):
		request.get_data()
		with SetData() as st:
			return st.setDataDB(request.data.decode())


api.add_resource(logins, '/logins', endpoint='logins') # Route_1
api.add_resource(upload1, '/uploads', endpoint='uploads') # Route_2
api.add_resource(menunya, '/menus', endpoint='menus') # Route_3
api.add_resource(showtablenya, '/showtable', endpoint='showtable') # Route_4
api.add_resource(reportnya, '/reports', endpoint='reports') # Route_5
api.add_resource(updatenya, '/updates', endpoint='updates') # Route_6

if __name__ == '__main__':
	app.run(port='5002')
