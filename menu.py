from querydb import QueryDB
from flask.json import jsonify


class menu:
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self

    def getData(self, params):
        with QueryDB() as qmenu:
            records = qmenu.menubro(params)
            return jsonify(records)
