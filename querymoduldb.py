from resultdb import ReslutDB


class QueryModulDB:
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self

    def qryselectdata(self, conn1, querynya, typeres, datefrom, dateto, typesnya, baseurls):
        global keydoc, headerdoc, pathfile, qry

        qry1 = querynya[0]['querys']
        
        tablenya1 = querynya[0]['tables'].split(", ")
        qryjoins = querynya[0]['query1'].split("@ ")
        joinparam = querynya[0]['joinParams'].split(", ")

        if querynya[0]['headers'] is not None and querynya[0]['headers'] != 'null':
            headerdoc = querynya[0]['headers']
        else:
            headerdoc = None

        if querynya[0]['pathFile'] is not None and querynya[0]['pathFile'] != 'null':
            pathFile = querynya[0]['pathFile']
        else:
            pathFile = None

        if querynya[0]['keys'] is not None and querynya[0]['keys'] != 'null':
            keydoc = querynya[0]['keys']
        else:
            keydoc = None

        if querynya[0]['header_Row_col'] is not None and querynya[0]['header_Row_col'] != 'null':
            header_Row_col = querynya[0]['header_Row_col']
        else:
            header_Row_col = None

        if querynya[0]['value_Row_col'] is not None and querynya[0]['value_Row_col'] != 'null':
            value_Row_col = querynya[0]['value_Row_col']
        else:
            value_Row_col = None

        qry = qry1 + tablenya1[0]
        for i in range(1, len(tablenya1)):
            qry += qryjoins[0] + tablenya1[i] + " " + qryjoins[1] + joinparam[i-1] + " " + qryjoins[2]

        if querynya[0]['whereParams'] is not None and querynya[0]['whereParams'] != 'null':
            if datefrom is not None and dateto is not None:
                qry += querynya[0]['whereParams'] + " '" + datefrom + "' AND '" + dateto + "' "
            else:
                qry += querynya[0]['whereParams'] + " "

        if querynya[0]['groupByParams'] is not None and querynya[0]['groupByParams'] != 'null':
            qry += querynya[0]['groupByParams'] + " "

        if querynya[0]['orderByParams'] is not None and querynya[0]['orderByParams'] != 'null':
            qry += querynya[0]['orderByParams']

        with ReslutDB() as rdb:
            result = rdb.toresult(conn1, qry, typeres, keydoc, headerdoc, header_Row_col, value_Row_col, datefrom, dateto, typesnya, baseurls, pathFile)
            return result