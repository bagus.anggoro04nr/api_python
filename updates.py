from flask.json import jsonify
from querydb import QueryDB


class SetData:
	
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self

    def setDataDB(self, params):
        with QueryDB() as qtabs:
            records = qtabs.updatedata(params)
            print(records)
            return jsonify(records)
