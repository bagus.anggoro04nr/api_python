from querydb import QueryDB
from flask.json import jsonify
import json


class ShowTables:
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self

    def getData(self, params):
        with QueryDB() as qtabs:
            records = qtabs.showtabs(params)
            # print(records)
            # data = json.loads(records)
            # print(data)
            return jsonify(records)
