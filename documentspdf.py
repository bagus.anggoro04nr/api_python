import pdfkit


class DocumentsPDF:

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self

    def toPDF(self, paramsheaders, paramskeys, paramsnya, datefrom, dateto, types, baseurls, pathFiles):
        _namafile = "Report_" + types + "_" + datefrom + "_" + dateto + ".pdf"
        _pathFile = "C:\\AppServ\\www\\doc_myapp\\"
        _dataHtml = _pathFile + "report.html"
        _FileNm = _pathFile + _namafile.replace(" ", "_").replace(":", "_").replace("-", "_")
        _datefrom = datefrom
        _dateto = dateto
        _types = types
        _baseurl1 = baseurls
        _pathfiles1 = pathFiles
        self.createFileHTML(paramsheaders, paramskeys, paramsnya, _pathFile, _datefrom, _dateto, _types)
        self.convert_html_to_pdf(_dataHtml, _FileNm)

    def createFileHTML(self, paramsheader, paramskey, params, _pathFile, _datefrom, _dateto, _types):
        global hargatotal1, totalQty1, totalhargaFull1
        paramnya = paramskey.format(":g")
        heads = paramsheader.format(":g").split(", ")
        myFile = open(_pathFile + "report.html", "w+")
        keyVal = ''
        count = 0
        totalQty = 0
        totalhargaFull = 0
        htmlString = "<!DOCTYPE html>"
        htmlString += "<html>"
        htmlString += "<head lang='en'>"
        htmlString += "<meta charset='UTF-8'>"
        htmlString += '<meta name="pdfkit-page-size" content="A4"/>'
        htmlString += '<meta name="pdfkit-orientation" content="Potrait"/>'
        htmlString += "<title>PDF REPORT</title>"
        htmlString += '<link rel="stylesheet" href="css/bootstrap.css">'
        htmlString += '<link rel="stylesheet" href="table.css">'
        htmlString += "</head>"
        htmlString += "<body>"
        htmlString += "<div class='container'>"
        htmlString += "<div class='card-header' style='backgroung-color: rgba(255,152,23, 0.7)'>"
        htmlString += "<h4><p>PT. @ XYZ</p></h4>"
        htmlString += "<h4><p>Report - " + _types.upper() + "</p></h4>"
        htmlString += "<h6><p>From Date : " + _datefrom + " To Date : " + _dateto + "</p></h6>"
        htmlString += "</div>"
        htmlString += "<div class=''>"
        htmlString += "<table class='table table-striped table-fixed' style='text-align: center' >"
        htmlString += "<thead>"
        htmlString += "<tr style='background-color: pink; '>"
        for header in heads:
            htmlString += "<th>" + header + "</th>"
        htmlString += "</tr>"
        htmlString += "</thead>"
        htmlString += "<tbody>"
        for key in range(len(params)):
            kode_po = params[key][paramnya]
            if keyVal != kode_po:
                count += 1
                htmlString += "<tr >"
                htmlString += "<td  >" + str(count) + "</td>"
                htmlString += "<td  >" + params[key]['kode_po'] + "</td>"
                htmlString += "<td  >" + params[key]['customer'] + "</td>"
                htmlString += "<td  >" + params[key]['alamat_cust'] + "</td>"
                namabarang1 = ''
                jmlhbarang1 = ''
                hargabeli1 = ''
                hargatotal = 0
                totalhargaFull2 = 0
                totalQty2 = 0
                for key1 in range(len(params)):
                    kode_ponya = params[key1][paramnya]
                    if kode_ponya == kode_po:
                        namabarang = "@ " + params[key1]['nama_barang'] + "</br>"
                        namabarang1 += namabarang
                        jmlhbarang = "@ " + str(params[key1]['jmlh_barang']) + "</br>"
                        jmlhbarang1 += jmlhbarang
                        harga_beli = "@ " + str(params[key1]['harga_beli']) + "</br>"
                        hargabeli1 += harga_beli
                        hargatotal += params[key1]['harga_beli']
                        hargatotal1 = str(hargatotal)
                        totalQty += params[key1]['jmlh_barang']
                        totalQty1 = totalQty
                        totalQty2 += params[key1]['jmlh_barang']
                        totalhargaFull += params[key1]['harga_beli']
                        totalhargaFull1 = totalhargaFull
                        totalhargaFull2 += params[key1]['harga_beli']
                htmlString += "<td  >" + namabarang1 + "</td>"
                htmlString += "<td  >" + jmlhbarang1 + "</td>"
                htmlString += "<td  >" + hargabeli1 + "</td>"
                htmlString += "<td  >" + hargatotal1 + "</td>"
                htmlString += "</tr>"
                htmlString += "<tr style='background-color: green;  '>"
                htmlString += "<th colspan='5'>Total Quantity : </th>"
                htmlString += "<td  >" + str(totalQty2) + "</td>"
                htmlString += "<th  >Total Harga</th>"
                htmlString += "<td  >" + str(totalhargaFull2) + "</td>"
                htmlString += "</tr>"
            keyVal = kode_po

        htmlString += "<tr style='background-color: cyan; '>"
        htmlString += "<th colspan ='5'>All Total Quantity : </th>"
        htmlString += "<td  >" + str(totalQty1) + "</td>"
        htmlString += "<th  >All Total Harga</th>"
        htmlString += "<td  >" + str(totalhargaFull1) + "</td>"
        htmlString += "</tr>"
        htmlString += "</tbody>"
        htmlString += "</table>"
        htmlString += "</div>"
        htmlString += "</div>"
        htmlString += "</body>"
        htmlString += "</html>"
        myFile.write(htmlString)
        myFile.close()

    def convert_html_to_pdf(self, souces_html, outputfile):
        _path_wkthmltopdf = "C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe"
        _config = pdfkit.configuration(wkhtmltopdf=_path_wkthmltopdf)
        _options2 = {
            'page-size': 'A4',
            'margin-top': '0.75in',
            'margin-right': '0.75in',
            'margin-bottom': '0.75in',
            'margin-left': '0.75in',
        }
        pdfkit.from_file(souces_html, outputfile, options=_options2, configuration=_config)
