from config import Config
from connmysql import Database
from querymoduldb import QueryModulDB
import json
from setget import SetGet


class QueryDB:
    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self

    def menubro(self, params):
       dbquery = Config().getQueryMenu()
       connec = dbquery['koneksidb']
       querys = self.qryselect(connec, dbquery['column'], dbquery['table'], dbquery['whereparam'], dbquery['limit'])
       with QueryModulDB() as qmdb:
           result = qmdb.qryselectdata(connec, querys, 0, None, None, None, None)
           return result

    def report(self, params):
        data = json.loads(params)
        with SetGet() as sg:
            sg.set__datetimefroms(data['datefrom'])
            sg.set__datetimes(data['dateto'])

            datefrom = sg.get__datetimefroms()
            dateto = sg.get__datetimes()

        dbquery = Config().getQueryReport(data['typesnya'])
        querys = self.qryselect(dbquery['koneksidb'], dbquery['column'], dbquery['table'], dbquery['whereparam'],
                                dbquery['limit'])
        with QueryModulDB() as qmdb:
            result = qmdb.qryselectdata(dbquery['koneksidata'], querys, data['typedoc'], datefrom, dateto, data['typesnya'], data['baseurls'])
            return result

    def updatedata(self, params):
        data = json.loads(params)
        with SetGet() as sg:
            sg.set__datetimefroms(data['datefrom'])
            sg.set__datetimes(data['dateto'])

            datefrom = sg.get__datetimefroms()
            dateto = sg.get__datetimes()

        dbquery = Config().getQueryReport(data['typesnya'])
        querys = self.qryselect(dbquery['koneksidb'], dbquery['column'], dbquery['table'], dbquery['whereparam'],
                                dbquery['limit'])
        with QueryModulDB() as qmdb:
            result = qmdb.qryselectdata(dbquery['koneksidata'], querys, data['typedoc'], datefrom, dateto, data['typesnya'], data['baseurls'])
            return result


    def showtabs(self, paramstabs):
        data = json.loads(paramstabs)
        with SetGet() as sg:
            sg.set__datetimefroms(data['datefrom'])
            sg.set__datetimes(data['dateto'])

            datefrom = sg.get__datetimefroms()
            dateto = sg.get__datetimes()

        dbquery = Config().getQueryShowtable(data['typesnya'])
        querys = self.qryselect(dbquery['koneksidb'], dbquery['column'], dbquery['table'], dbquery['whereparam'],
                                dbquery['limit'])
        with QueryModulDB() as qmdb:
            result = qmdb.qryselectdata(dbquery['koneksidata'], querys, 0, datefrom, dateto, None, None)
            return result

    def qryselect(self, conn, column, tables, whereparams, limits):
        qry = "SELECT " + column + " FROM " + tables + " WHERE " + whereparams + " " + limits + ";"
        with Database(conn) as db:
            records = db.query(qry)
            db.close()
            return records

