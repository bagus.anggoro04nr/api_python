


class Config:

        def __init__(self):
                self._server = 'localhost'
                self._user = 'root'
                self._pass = '123456789'
                self._db = 'myapp_admin, myapp_inventory, myapp_karyawan'
                self._table = 'app_query'
                self._column = '*'

        def __enter__(self):
                return self

        def __exit__(self, exc_type, exc_val, exc_tb):
                return self

        def getKoneksi(self, params):
                db = self._db.split(", ")
                DATABASE_CONFIG = {'server': self._server, 'user': self._user, 'password': self._pass, 'dbname': db[params]}
                return DATABASE_CONFIG

        def getQueryMenu(self):
                params = {
                        'table': self._table,
                        'column': self._column,
                        'whereparam': "type_query = 'menu' AND nama_query = 'selectmenu'",
                        'koneksidb': 0,
                        'limit': 'LIMIT 1'}
                return params

        def getQueryReport(self, typer):
                params = {
                        'table': self._table,
                        'column': self._column,
                        'whereparam': "type_query = '" + typer + "' AND nama_query = 'reports'",
                        'koneksidb': 0,
                        'koneksidata': 1,
                        'limit': 'LIMIT 1'}
                return params

        def getQueryShowtable(self, typer):
                params = {
                        'table': self._table,
                        'column': self._column,
                        'whereparam': "type_query = '" + typer + "' AND nama_query = 'showtable'",
                        'koneksidb': 0,
                        'koneksidata': 1,
                        'limit': 'LIMIT 1'}
                return params

        def getQueryEdit(self, typer):
                params = {
                        'table': self._table,
                        'column': self._column,
                        'whereparam': "type_query = '" + typer + "' AND nama_query = 'edit'",
                        'koneksidb': 0,
                        'koneksidata': 1,
                        'limit': 'LIMIT 1'}
                return params

        def getQueryDetail(self, typer):
                params = {
                        'table': self._table,
                        'column': self._column,
                        'whereparam': "type_query = '" + typer + "' AND nama_query = 'detail'",
                        'koneksidb': 0,
                        'koneksidata': 1,
                        'limit': 'LIMIT 1'}
                return params

        def getQueryUpdateTable(self, typer):
                params = {
                        'table': self._table,
                        'column': self._column,
                        'whereparam': "type_query = '" + typer + "' AND nama_query = 'updatetable'",
                        'koneksidb': 0,
                        'koneksidata': 1,
                        'limit': 'LIMIT 1'}
                return params