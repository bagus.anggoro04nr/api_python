from config import Config
import mysql.connector
from mysql.connector import Error

class Database:

    def __init__(self, params):
        dbkonek = Config().getKoneksi(params)
        self._conn = mysql.connector.connect(
            host=dbkonek['server'],
            database=dbkonek['dbname'],
            user=dbkonek['user'],
            password=dbkonek['password']
            )
        self._cursor = self._conn.cursor(dictionary=True)

    @property
    def connection(self):
        return self._conn

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def error(self):
        return Error

    def commit(self):
        self.connection.commit()

    def close(self, commit=True):
        if commit:
            self.commit()
        self.connection.close()

    def execute(self, sql, params=None):
        self._cursor.execute(sql, params or ())

    def fetchall(self):
        return self._cursor.fetchall()

    def fetchone(self):
        return self._cursor.fetchone()

    def query(self, sql, params=None):
        self._cursor.execute(sql, params or ())
        return self.fetchall()
